<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Active People</title>
    <head>
        <link rel="stylesheet" href="<%= request.getContextPath()%>/resources/css/bootstrap.min.css">
        <link rel="stylesheet" href="<%= request.getContextPath()%>/resources/css/peeps.css">
    </head>
</head>
<body>

    <div class="container">

        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <a class="brand" href="#">Peeps</a>
                <ul class="nav">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#">Link</a></li>
                    <li><a href="#">Link</a></li>
                </ul>
            </div>
        </div>

        <div class="row">

            <div class="span12">
                <h3>Active People</h3>
                <table class="table table-striped table-bordered responsive">
                    <thead>
                        <th>Id</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </thead>
                    <tbody>
                    <c:forEach var="person" items="${activePeople}">
                    <tr>
                        <td>${person.id}</td>
                        <td>${person.firstName}</td>
                        <td>${person.lastName}</td>
                    </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</body>
</html>