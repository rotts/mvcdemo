package edu.uwf.service;

import edu.uwf.dao.OraclePersonDao;
import edu.uwf.entities.Person;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class PersonService {

    @Autowired
    OraclePersonDao personDao;

    public void setPersonDao(OraclePersonDao personDao){
        this.personDao = personDao;
    }

    /*
    public void create(Person person){
        this.personDao.create(person);
    }
    */
    public Person getPerson(Integer id){
        return this.personDao.getPerson(id);
    }

    /*
    public List<Person> getAllPeople(){
        return (List<Person>) this.personDao.findPersons(1, 50);
    }

    public List<Person> getActivePeople(){
        return this.personDao.getActivePersons();
    }
    */
}
