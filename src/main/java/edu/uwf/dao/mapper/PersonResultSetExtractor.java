package edu.uwf.dao.mapper;

import edu.uwf.entities.Person;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User: rotts
 * Date: 3/9/14
 * Time: 10:00 PM
 */

public class PersonResultSetExtractor implements ResultSetExtractor {

    @Override
    public Object extractData(ResultSet rs) throws SQLException {
        Person person = new Person();
        person.setFirstName(rs.getString("FIRST_NAME"));
        person.setLastName(rs.getString("LAST_NAME"));
        person.setId(rs.getInt("EMPLOYEE_ID"));
        return person;
    }
}
