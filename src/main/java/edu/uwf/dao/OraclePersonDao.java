package edu.uwf.dao;

import com.jolbox.bonecp.BoneCPDataSource;
import edu.uwf.dao.mapper.PersonRowMapper;
import edu.uwf.entities.Person;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: rotts
 * Date: 3/9/14
 * Time: 9:19 PM
 */

public class OraclePersonDao {

    private BoneCPDataSource oracleDataSource;

    @PersistenceContext
    public void setOracleDataSource(BoneCPDataSource oracleDataSource) {
        this.oracleDataSource = oracleDataSource;
    }

    public BoneCPDataSource getOracleDataSource() {
        return oracleDataSource;
    }

    public Person getPerson(int id) {

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("id", id);

        String sql = "select * from HR.EMPLOYEES where EMPLOYEE_ID = :id ";

        NamedParameterJdbcTemplate select = new NamedParameterJdbcTemplate(this.oracleDataSource);
        List<Person> person = select.query(sql, parameters, new PersonRowMapper());

        return person.get(0);
    }
}
