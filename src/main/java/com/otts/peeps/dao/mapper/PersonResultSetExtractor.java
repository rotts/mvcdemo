package com.otts.peeps.dao.mapper;

/**
 * User: rotts
 */

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.ResultSetExtractor;

import com.otts.peeps.Person;

public class PersonResultSetExtractor implements ResultSetExtractor {

    @Override
    public Object extractData(ResultSet rs) throws SQLException {
        Person person = new Person();
        person.setFirstName(rs.getString("firstname"));
        person.setLastName(rs.getString("lastname"));
        person.setMiddleName(rs.getString("middlename"));
        person.setSuffix(rs.getString("suffix"));
        person.setTitle(rs.getString("title"));
        person.setId(rs.getInt("id"));
        return person;
    }

}