package com.otts.peeps.dao;

import com.otts.peeps.Person;

import java.util.List;

/**
 * User: rotts
 */
public interface IPersonDao {

    public void create(Person person);
    public Person getPerson(int id);
    public List<Person> findPersons(final int startIndex, final int maxResults);
    public List<Person> getActivePersons();
}
