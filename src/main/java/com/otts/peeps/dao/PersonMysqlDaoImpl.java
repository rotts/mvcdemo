package com.otts.peeps.dao;

import com.otts.peeps.Person;

import com.otts.peeps.dao.mapper.PersonRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Repository("personDao")
public class PersonMysqlDaoImpl implements IPersonDao {

    @Autowired
    private DataSource dataSource;

    @PersistenceContext
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void create(Person person) {

        JdbcTemplate insert = new JdbcTemplate(dataSource);
        insert.update("INSERT INTO person (title, firstName, lastName, middleName, suffix) VALUES(?,?,?,?,?)",
                new Object[]{
                        person.getTitle(),
                        person.getFirstName(),
                        person.getLastName(),
                        person.getMiddleName(),
                        person.getSuffix()
                });
    }

    public Person getPerson(int id) {

        JdbcTemplate select = new JdbcTemplate(dataSource);
        List<Person> person = select.query("select id, title, firstname, lastname, middlename, suffix from person where id = ?",
                new Object[] { id },
                new PersonRowMapper());

        return person.get(0);
    }

    public List<Person> findPersons(final int startIndex, final int maxResults) {

        JdbcTemplate select = new JdbcTemplate(dataSource);
        List people;
        people = select.query("select id, title, firstname, lastname, middlename, suffix from person",
                new PersonRowMapper());

        return people;
    }

    public List<Person> getActivePersons(){

        SimpleJdbcCall activeSp =  new SimpleJdbcCall(new JdbcTemplate(this.dataSource))
                .withProcedureName("getPeopleByStatus")
                .returningResultSet("peopleByStatus", new PersonRowMapper());

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("statusId", 1);

        Map<String, Object> m = activeSp.execute(params);
        return (List<Person>) m.get("peopleByStatus");
    }
}