package com.otts.peeps.controller;

import edu.uwf.entities.Person;
import edu.uwf.service.PersonService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;


@Controller
@RequestMapping("/")
public class AppController {

    private static final Logger logger = Logger.getLogger(AppController.class);

    @Autowired
    private PersonService personService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView home(){

        logger.info("index");

        ModelAndView model = new ModelAndView("index");

        return model;
    }

    @RequestMapping(value = "person/{id}", method = RequestMethod.GET)
    public ModelAndView personById(@PathVariable Integer id){

        Person person = this.personService.getPerson(id);
        ModelAndView model = new ModelAndView("people/activePeople");
        List<Person> people = new ArrayList<Person>();
        people.add(person);
        model.addObject("activePeople", people);

        return model;
    }

    @RequestMapping(value = "diag", method = RequestMethod.GET)
    public ModelAndView diag(){

        logger.info("diag");
        this.personService.getPerson(1);
        ModelAndView model = new ModelAndView("diag");

        return model;
    }

}
