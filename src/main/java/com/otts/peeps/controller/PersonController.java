package com.otts.peeps.controller;


import javax.servlet.http.HttpServletRequest;

import com.otts.peeps.Person;
import com.otts.peeps.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * User: rotts
 */

@Controller
@RequestMapping("/people")
public class PersonController {

    @Autowired
    private PersonService personService;

    public void setPersonService(PersonService personService){
        this.personService = personService;
    }

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public @ResponseBody List<Person> getAll(HttpServletRequest request){

        List<Person> people = personService.getAllPeople();
        return people;
    }

    @RequestMapping(value = "id/{id}", method = RequestMethod.GET)
    public @ResponseBody Person getPerson(@PathVariable String id){

        Person person = personService.getPerson(Integer.valueOf(id));
        return person;
    }

    @RequestMapping(value = "new", method = RequestMethod.POST, consumes="application/json")
    public @ResponseBody Boolean getAll(@RequestBody Person person){

        personService.create(person);
        return true;
    }

    @RequestMapping(value = "active", method = RequestMethod.GET)
    public String getActivePeople(Model model){

        model.addAttribute("activePeople", personService.getActivePeople());

        return "people/activePeople";
    }
}
