package com.otts.peeps;

import com.otts.peeps.dao.IPersonDao;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class PersonService {

    @Autowired
    IPersonDao personDao;

    public void setPersonDao(IPersonDao personDao){
        this.personDao = personDao;
    }

    public void create(Person person){
        this.personDao.create(person);
    }

    public Person getPerson(Integer id){

        return this.personDao.getPerson(id);
    }

    public List<Person> getAllPeople(){
        return (List<Person>) this.personDao.findPersons(1, 50);
    }

    public List<Person> getActivePeople(){
        return this.personDao.getActivePersons();
    }
}
